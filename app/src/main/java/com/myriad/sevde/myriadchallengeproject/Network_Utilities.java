package com.myriad.sevde.myriadchallengeproject;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.myriad.sevde.myriadchallengeproject.Quest_Structure;

public class Network_Utilities {

    //If there is no internet connection, display AlertDialog
    public static Dialog alertInternet(final Context contex){

        AlertDialog.Builder dialogConnection=new AlertDialog.Builder(contex);
        dialogConnection.setMessage(contex.getResources().getString(R.string.dialog_text));
        dialogConnection.setTitle(contex.getResources().getString(R.string.dialog_title));
        dialogConnection.setPositiveButton(contex.getResources().getString(R.string.button_cancel),new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                System.exit(0);
            }
        });
        dialogConnection.setNegativeButton(contex.getResources().getString(R.string.button_okay),null);
        return dialogConnection.create();

    }

    public static void readQuestList (ArrayList<Quest_Structure> mQuestList) throws IOException {

        System.setProperty ("jsse.enableSNIExtension", "false");
        URL url = new URL("https://staging.myriaddevices.com/dropfiles/challenges/android/quest_list.txt");
        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

        String str;
        String initString = "List of quests:";
        String Title = null;
        String Alignment = null;
        String Description= null;
        String Location = null;
        Double LocationX = 0.0;
        Double LocationY = 0.0;
        String QuestGiver= null;

        while ( (str = in.readLine())!= null   ) {
            Log.d("msg", "read start ");

            //Trim the blank lines
            while((str.isEmpty()|| str.trim().equals("") || str.trim().equals("\n"))) {
                str = in.readLine().trim();
            }

            //Find the initial line
            if( str.equals(initString)) {
                str = in.readLine().trim();
                //trim blank lines
                while((str.isEmpty() || str.trim().equals("") || str.trim().equals("\n"))) {
                    str = in.readLine().trim();
                }
            }

            if( str.lastIndexOf("alignment")>=0) {
                int firstIndex = str.indexOf(":") + 1;
                int lastIndex  = str.length()-1;
                Alignment = str.substring(firstIndex, lastIndex).trim();
            }

            else if( str.indexOf("description")>=0) {
                int firstIndex = str.indexOf(":") + 3;
                int lastIndex  = str.length()-2;
                Description = str.substring(firstIndex, lastIndex).trim();
            }

            else if( str.indexOf("location")>=0) {
                int firstIndex = str.indexOf("(") + 1;
                int midIndex   = str.indexOf(",") ;
                int lastIndex  = str.indexOf(")");
                Location =  str.substring(firstIndex-1,lastIndex+1).trim();
                LocationX = Double.parseDouble(str.substring(firstIndex, midIndex)) ;
                LocationY = Double.parseDouble(str.substring(midIndex+1, lastIndex)) ;
            }

            else if( str.indexOf("QuestGiver")>=0) {
                int firstIndex = str.indexOf(":") + 1;
                int lastIndex  = str.indexOf("(");
                QuestGiver = str.substring(firstIndex, lastIndex).trim();

                Quest_Structure quest = new Quest_Structure();
                quest.setTitle(Title);
                quest.setAlianment(Alignment);
                quest.setDescription(Description);
                quest.setLocation(Location);
                quest.setLocationX(LocationX);
                quest.setLocationY(LocationY);
                quest.setQuestGiver(QuestGiver);

                //Log.d("msg", quest.getAlianment());
                mQuestList.add(quest);
            }

            else { Title = str.replace(":", "");}
        }

        in.close();
        return ;
    }
}
 