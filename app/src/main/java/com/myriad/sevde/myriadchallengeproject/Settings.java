package com.myriad.sevde.myriadchallengeproject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

import com.myriad.sevde.myriadchallengeproject.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Settings extends Activity{
    private Spinner alignments;
    String details=null;
    String alignment=null;
    private ArrayList<Quest_Structure> newQuest = new ArrayList<Quest_Structure>();
    private ArrayList<String> questTitle = new ArrayList<String>();
    private ArrayList<String> questAlignment = new ArrayList<String>();
    private AutoCompleteTextView autoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_settings);
        new getData().execute();
        findViewById(R.id.btnUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Show_Listview.showDetails.finish();
                Intent intent = new Intent(Settings.this,Filtered_Quest_List.class);
                intent.putExtra("Filter", alignments.getSelectedItem().toString());
                startActivity(intent);
                return;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            default:
                return  super.onOptionsItemSelected(item);
        }
    }

    public void displayQuestinTextview() {

        for (int i = 0; i < newQuest.size(); i++) {
            details = newQuest.get(i).getTitle();
            alignment=newQuest.get(i).getAlianment();
            questTitle.add(details);
            questAlignment.add(alignment);
            details = "";
        }

        Collections.sort(questAlignment);
        Collections.reverse(questAlignment);
        autoList = (AutoCompleteTextView) findViewById(R.id.listOfQuest);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.list_view_for_quest, questTitle);
        autoList.setAdapter(arrayAdapter);
        autoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(Settings.this, Present_Quests.class);
                intent.putExtra("structure", newQuest.get(position));
                startActivity(intent);
                return;
            }
        });

        alignments = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(this, R.layout.list_view_for_quest, questAlignment);
        alignments.setAdapter(arrayAdapter1);
    }

    class getData extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Network_Utilities.readQuestList(newQuest);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            displayQuestinTextview();
        }
    }
}
