package com.myriad.sevde.myriadchallengeproject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.myriad.sevde.myriadchallengeproject.R;

import java.io.IOException;
import java.util.ArrayList;

import static com.myriad.sevde.myriadchallengeproject.R.layout.activity_filtered_quest__list;

public class Filtered_Quest_List extends Activity {

    String details=null;
    String alignment=null;
    private ArrayList<Quest_Structure> newQuest = new ArrayList<Quest_Structure>();
    private ArrayList<String> questTitle = new ArrayList<String>();
    private String value=null;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtered_quest__list);
        getActionBar().setHomeButtonEnabled(true);
        intent=getIntent();
        new BackgroundTaskFromSettings().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.show__listview, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                startActivity(new Intent(Filtered_Quest_List.this, Settings.class));
                return true;
            case R.id.action_logout:
                startActivity(new Intent(Filtered_Quest_List.this,MyActivity.class));
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public  void displayFilteredData(){
        value=intent.getStringExtra("Filter");
        if(value.equals("NEUTRAL")){
            setTitle("NEUTRAL");
            for(int i=0; i<newQuest.size(); i++){
                details=newQuest.get(i).getTitle()+"\n"+ newQuest.get(i).getQuestGiver() ;
                questTitle.add(details);
                details="";
            }
        }
        else if(value.equals("GOOD")){
            setTitle("GOOD");
            for(int i=0; i<newQuest.size(); i++){
                details=newQuest.get(i).getTitle()+"\n"+ newQuest.get(i).getQuestGiver();
                if(newQuest.get(i).getAlianment().equals("GOOD")){
                    questTitle.add(details);
                    details="";
                }
            }
        }
        else if(value.equals("EVIL")){
            setTitle("EVIL");
            for(int i=0; i<newQuest.size(); i++){
                details=newQuest.get(i).getTitle()+"\n"+ newQuest.get(i).getQuestGiver();
                if(newQuest.get(i).getAlianment().equals("EVIL")){
                    questTitle.add(details);
                    details="";
                }
            }
        }
        ListView listView = (ListView)findViewById(R.id.quest_listfiltered);
        ArrayAdapter<String> arrayAdapter= new ArrayAdapter<String>(this, R.layout.list_view_for_quest,questTitle);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(Filtered_Quest_List.this, Present_Quests.class);
                intent.putExtra("structure",newQuest.get(position));
                startActivity(intent);
                return;
            }
        });
    }

    class BackgroundTaskFromSettings extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                Network_Utilities.readQuestList(newQuest);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            displayFilteredData();
        }
    }

}
