package com.myriad.sevde.myriadchallengeproject;
import java.io.Serializable;

public class Quest_Structure implements Serializable {
    private String title;
    private String alignment;
    private String description;
    private String location;
    private Double locationX;
    private Double locationY;
    private String questGiver;

    public void setTitle(String Title) { title = Title; }

    public void setAlianment(String Alignment)  {
        alignment = Alignment;
    }

    public void setDescription(String Description)  {
        description = Description;
    }

    public void setLocation(String Location) { location = Location; }

    public void setLocationX(Double LocationX)  {
        locationX = LocationX;
    }

    public void setLocationY(Double LocationY)  {
        locationY = LocationY;
    }

    public void setQuestGiver(String QuestGiver)  {
        questGiver = QuestGiver;
    }

    public String getAlianment()  {
        return alignment;
    }

    public String getDescription()  {
        return description;
    }

    public String getLocation()  {
        return location;
    }

    public Double getLocationX() {
        return locationX;
    }

    public Double getLocationY()  {
        return locationY;
    }

    public String getQuestGiver()  {
        return questGiver;
    }

    public String getTitle()  {
        return title;
    }

}
