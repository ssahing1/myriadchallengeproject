package com.myriad.sevde.myriadchallengeproject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import static android.view.View.OnClickListener;

public class MyActivity extends Activity {
    private CheckBox chkRemember;
    private EditText userName;
    private EditText userPassword;
    SharedPreferences myPreference;
    private Boolean saveLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        userName=(EditText)findViewById(R.id.edtUsername);
        userPassword=(EditText)findViewById(R.id.edtPassword);
        chkRemember=(CheckBox)findViewById(R.id.chkRemember);
        myPreference=getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor edit=myPreference.edit();

        saveLogin=myPreference.getBoolean("rememberMe",false);
        if(saveLogin==true){
            userName.setText(myPreference.getString("username",""));
            chkRemember.setChecked(true);
            userPassword.requestFocus();
        }
        initilizeUIobjects();
    }

    private void saveLoginUsername(){
        String username= userName.getText().toString().trim();
        SharedPreferences.Editor edit=myPreference.edit();
        if(chkRemember.isChecked() && username.contains("Lancelot")){
            edit.putBoolean("rememberMe",true);
            edit.putString("username",username);
            edit.commit();
        }
        else{
            edit.clear();
            edit.commit();
            //userName.requestFocus();
        }
    }

    private void initilizeUIobjects() {
        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                    saveLoginUsername();
                    performLogin();
            }
        });
    }

    private void performLogin() {
        String username=userName.getText().toString().trim();
        String password=userPassword.getText().toString().trim();
        if(username.equals("Lancelot") && password.equals("arthurDoesntKnow")){
            startActivity(new Intent(this,Show_Listview.class));
            /*userName.setText("");
            userPassword.setText("");*/
        }

        else if(!(username.equals("Lancelot"))|| TextUtils.isEmpty(username)){
            userName.setError(getResources().getString(R.string.errUsername));
            userName.setText("");
            //return;
        }

        else if(!(password.equals("arthurDoesntKnow")) || TextUtils.isEmpty(password)){
            userPassword.setError(getResources().getString(R.string.errPassword));
            userPassword.setText("");
            //return;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return true;
        }
        return false;
    }

}
