package com.myriad.sevde.myriadchallengeproject;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.internal.ge;
import com.google.android.gms.internal.s;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.myriad.sevde.myriadchallengeproject.R;

import static android.R.layout.simple_spinner_dropdown_item;

public class Present_Quests extends Activity {
    private GoogleMap googleMap;
    private MarkerOptions marker;
    private CameraPosition cameraPos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_present_quests);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        try{initMap();}catch (Exception e){e.printStackTrace();}
        showDetails();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initMap();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.show__listview, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                startActivity(new Intent(Present_Quests.this, Settings.class));
                return true;
            case R.id.action_logout:
                startActivity(new Intent(Present_Quests.this,MyActivity.class));
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected  void initMap(){
        if(googleMap==null){
            googleMap=((MapFragment)getFragmentManager().findFragmentById(R.id.fragmentMap)).getMap();
            googleMap.setMyLocationEnabled(true);
            if(googleMap==null){
                Toast.makeText(this,"Unable to create the map!",Toast.LENGTH_LONG).show();
            }
            else showPositions();
        }
        else showPositions();
    }

    protected  void showPositions(){
        Intent intent=getIntent();
        final Quest_Structure structure=(Quest_Structure)intent.getSerializableExtra("structure");
        marker=new MarkerOptions().position(new LatLng(structure.getLocationX(), structure.getLocationY()))
                .title(structure.getTitle().toString());
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).alpha(0.7f);
        cameraPos=new CameraPosition.Builder()
                .target(new LatLng(structure.getLocationX(), structure.getLocationY()))
                .zoom(3).build();
        googleMap.addMarker(marker);
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPos));
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Uri location = Uri.parse("geo:"+Double.toString(structure.getLocationX())+","+Double.toString(structure.getLocationY())+"?z=10");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);
                startActivity(mapIntent);
                return true;
            }
        });
    }

    private void showDetails() {
        Intent intent=getIntent();
        Quest_Structure structure=(Quest_Structure)intent.getSerializableExtra("structure");
        TextView txtQuest=(TextView)findViewById(R.id.quest_detail);
        txtQuest.setText(structure.getDescription()+"\n");
        txtQuest.setMovementMethod(new ScrollingMovementMethod());
    }

}
