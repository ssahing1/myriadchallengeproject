package com.myriad.sevde.myriadchallengeproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

public class Show_Listview extends Activity{
    public static Activity showDetails;
    String details=null;
    String alignment=null;
    private ArrayList<Quest_Structure> newQuest = new ArrayList<Quest_Structure>();
    private ArrayList<String> questTitle = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayHomeAsUpEnabled(false);
        getActionBar().setHomeButtonEnabled(false);
        setContentView(R.layout.activity_show__listview);
        showDetails=this;
        if(isOnline()){
            new BackgroundTask().execute();
        }
        else Network_Utilities.alertInternet(Show_Listview.this).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.show__listview, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                startActivity(new Intent(Show_Listview.this, Settings.class));
                return true;
            case R.id.action_logout:
                startActivity(new Intent(Show_Listview.this,MyActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void displayQuest(){

        for(int i=0; i<newQuest.size(); i++){
            details=newQuest.get(i).getTitle()+"\n"+ newQuest.get(i).getQuestGiver() ;
            questTitle.add(details);
            details="";
        }

        ListView listView = (ListView)findViewById(R.id.quest_list);
        ArrayAdapter<String> arrayAdapter= new ArrayAdapter<String>(this, R.layout.list_view_for_quest,questTitle);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(Show_Listview.this, Present_Quests.class);
                intent.putExtra("structure",newQuest.get(position));
                startActivity(intent);
                return;
            }
        });
    }

    //Check if the device is connected to the internet
    public Boolean isOnline(){
        ConnectivityManager conManager=(ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo=conManager.getActiveNetworkInfo();
        if(netInfo==null || !netInfo.isConnected() || !netInfo.isAvailable()){
            return false;
        }
        return true;
    }

    ProgressDialog lProgressDialog;
    class BackgroundTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            lProgressDialog = new ProgressDialog(Show_Listview.this);
            lProgressDialog.setMessage("Please wait");
            lProgressDialog.setCancelable(false);
            lProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                Network_Utilities.readQuestList(newQuest);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            lProgressDialog.cancel();
            displayQuest();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            startActivity(new Intent(Show_Listview.this,MyActivity.class));
            return true;
        }
        return false;
    }
}
